//
// Created by tevin on 11/7/16.
//

#ifndef BANKACCOUNTSUBMIT_BANK_H_H
#define BANKACCOUNTSUBMIT_BANK_H_H
class Bank {
private:
    int accountnumber;
    double accountbalance;
    const static double annualinterest;
public:
    void enterAccountData();
    void computeInterest(int years);
    void displayAccount();

};
#endif //BANKACCOUNTSUBMIT_BANK_H_H
