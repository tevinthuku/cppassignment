//
// Created by tevin on 11/7/16.
//

#include <iostream>

#include "Bank.h"

using namespace std;

const double Bank::annualinterest = 0.03;

void Bank::enterAccountData() {

    do{
        cout << "The account number is " << endl;
        cin >> accountnumber;
    }while(accountnumber< 1000);

//    accountnumber = 0;
//    while(accountnumber<1000){
//        cout << "The account number is " << endl;
//        cin >> accountnumber;
//    }


    do{
        cout << "The account balance is " << endl;
        cin >> accountbalance;
    }while(accountbalance < 0);
}

void Bank::computeInterest(int years) {

    accountbalance = accountbalance + (accountbalance * annualinterest * years);
}

void Bank::displayAccount() {
    cout << "The account number is " << accountnumber << endl;
    cout << "The account balance is " << accountbalance << endl;
}

