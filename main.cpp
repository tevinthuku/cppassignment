#include <iostream>

#include "Bank.h"
using namespace std;

int years, quit;
int main() {
    Bank Account[10];

    // Account[1]
    // Account[2]
    // Account[3]

    for(int i = 1; i<=10; i++) {
        cout << "Account " << i << endl;
        do{
            cout << "Enter the number of years account  " << i <<" has stayed in the bank "<< endl;
            cin >> years;
        }while(years < 1 || years > 40);

        Account[i].enterAccountData();
        Account[i].computeInterest(years);
        Account[i].displayAccount();

        // quit logic

        cout << "Do you want to quit. Press 1 to quit or press any other number to cont " << endl;
        cin >> quit;

        if(quit == 1) break;

    }
    return 0;
}